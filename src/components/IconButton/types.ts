import { ButtonHTMLAttributes, DetailedHTMLProps, ReactNode } from "react"

export type IconButtonType = {
  icon: ReactNode
} & DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>
