import { IconButtonType } from "./types";

export function IconButton({ icon, onClick }: IconButtonType) {
  return (
    <button className="icon-button" onClick={onClick}>
      {icon}
    </button>
  )
}
