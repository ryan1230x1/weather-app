import { ReactNode } from "react"
import { GroupDayObject } from "../../types";

export type ModalType = {
  isOpen: boolean;
  onClose: () => void
  data: GroupDayObject | undefined
  header: (data: GroupDayObject | undefined) => ReactNode;
  body: (data: GroupDayObject | undefined) => ReactNode;
}
