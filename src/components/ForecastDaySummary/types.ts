export type ForecastDaySummaryType = {
  title: string,
  icon: string,
  maxTemperature: number,
  minTemperature: number
  handleShowDetails: () => void
}
