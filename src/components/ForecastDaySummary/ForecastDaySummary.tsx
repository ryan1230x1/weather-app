import ArrowCircle from "../../assets/icons/ArrowCircle"
import { getWeatherIcon, getWeatherShortDescription } from "../../helpers"
import IconButton from "../IconButton"
import WeatherIcon from "../WeatherIcon"
import { ForecastDaySummaryType } from "./types"

export function ForecastDaySummary({
  title,
  icon,
  maxTemperature,
  minTemperature,
  handleShowDetails
}: ForecastDaySummaryType) {
  return (
    <div className="forecast-day-summary">
      <h2>{title}-{getWeatherShortDescription(icon)}</h2>
      <div>
        <div className="forecast-day-summary-content">
          <WeatherIcon
            shortDescription={getWeatherShortDescription(icon)}
            icon={getWeatherIcon(icon)} />
          <p>
            <span>{minTemperature}<span className="celcius">&deg;</span></span>
            {" / "}
            <span>{maxTemperature}<span className="celcius">&deg;</span></span>
          </p>
          <IconButton onClick={handleShowDetails} icon={<ArrowCircle color="white" />} />
        </div>
      </div>
    </div>
  )
}
