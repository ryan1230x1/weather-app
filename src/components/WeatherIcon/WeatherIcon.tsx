import { WeatherIconType } from "./types";

export function WeatherIcon({ icon, shortDescription, width = 50, height = 50 }: WeatherIconType) {
  return (
    <img
      width={width}
      height={height}
      alt={shortDescription + " weather icon"}
      aria-label={shortDescription + " weather icon"}
      src={`http://openweathermap.org/img/w/${icon}.png `}
    />
  )
}
