import { DetailedHTMLProps, ImgHTMLAttributes } from "react"

export type WeatherIconType = {
  icon: string
  shortDescription: string,
} & DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>

