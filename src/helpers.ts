import { List } from "./types";

export function getDayName(datetime: string, type: Intl.DateTimeFormatOptions["weekday"]) {
  var date = new Date(datetime);
  return date.toLocaleDateString("en-GB", { weekday: type });
}

export function getMinTempAndMaxTemp(entries: List[]) {
  const temperture = new Set<number>([])
  for (const row of entries) {
    temperture.add(row.main.temp)
  }
  return {
    min: Math.ceil(Math.min(...Array.from(temperture))),
    max: Math.ceil(Math.max(...Array.from(temperture))),
  }
}

export function getAverageWeatherForcast(entries: List[]) {
  const count: { [key: string]: number } = {}
  const weatherArray = []

  for (const entry of entries) {
    weatherArray.push(`${entry.weather[0].main}-${entry.weather[0].icon}`)
  }

  weatherArray.forEach((value) => {
    count[value] = (count[value] || 0) + 1
  })

  const averageWeather = Math.max(...Object.values(count))
  return Object.keys(count).filter((key) => count[key] === averageWeather)[0]
}

export function getHourlyForcastForADay(entries: List[], datetime: string) {
  return entries.filter((entry) => entry.dt_txt.includes(datetime))
}

export function getWeatherShortDescription(weather: string) {
  return weather?.split("-")[0]
}

export function getWeatherIcon(weather: string) {
  return weather?.split("-")[1]
}

export function formatDateToHourAmOrPm(datetime: string) {
  return new Date(datetime).toLocaleString("en-US", {
    hour12: true,
    hour: "numeric"
  })
}

export function getApiUrl(location: string) {
  return `${process.env.REACT_APP_API_URL}?&q=${location}&appid=${process.env.REACT_APP_API_KEY}&units=metric`
}
