import { rest } from 'msw'

const API_PATH = `${process.env.REACT_APP_API_URL}?&q=London&appid=${process.env.REACT_APP_API_KEY}&units=metric`

export const handlers = [
  // Handles a GET request to the weather API
  rest.get(API_PATH, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        "cod": "200",
        "message": 0,
        "cnt": 40,
        "list": [
          {
            "dt": 1671246000,
            "main": {
              "temp": -3.64,
              "feels_like": -3.64,
              "temp_min": -3.64,
              "temp_max": -0.95,
              "pressure": 1022,
              "sea_level": 1022,
              "grnd_level": 1018,
              "humidity": 94,
              "temp_kf": -2.69
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 97
            },
            "wind": {
              "speed": 1.27,
              "deg": 210,
              "gust": 2.87
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-17 03:00:00"
          },
          {
            "dt": 1671256800,
            "main": {
              "temp": -2.64,
              "feels_like": -4.66,
              "temp_min": -2.64,
              "temp_max": -0.65,
              "pressure": 1022,
              "sea_level": 1022,
              "grnd_level": 1018,
              "humidity": 93,
              "temp_kf": -1.99
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 97
            },
            "wind": {
              "speed": 1.44,
              "deg": 179,
              "gust": 1.98
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-17 06:00:00"
          },
          {
            "dt": 1671267600,
            "main": {
              "temp": -0.99,
              "feels_like": -3.34,
              "temp_min": -0.99,
              "temp_max": 0.34,
              "pressure": 1022,
              "sea_level": 1022,
              "grnd_level": 1019,
              "humidity": 90,
              "temp_kf": -1.33
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04d"
              }
            ],
            "clouds": {
              "all": 99
            },
            "wind": {
              "speed": 1.8,
              "deg": 176,
              "gust": 4.19
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-17 09:00:00"
          },
          {
            "dt": 1671278400,
            "main": {
              "temp": 2.89,
              "feels_like": -0.12,
              "temp_min": 2.89,
              "temp_max": 2.89,
              "pressure": 1022,
              "sea_level": 1022,
              "grnd_level": 1019,
              "humidity": 85,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 3.11,
              "deg": 198,
              "gust": 11.81
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-17 12:00:00"
          },
          {
            "dt": 1671289200,
            "main": {
              "temp": 2.16,
              "feels_like": -0.31,
              "temp_min": 2.16,
              "temp_max": 2.16,
              "pressure": 1022,
              "sea_level": 1022,
              "grnd_level": 1019,
              "humidity": 89,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04d"
              }
            ],
            "clouds": {
              "all": 98
            },
            "wind": {
              "speed": 2.34,
              "deg": 193,
              "gust": 7.55
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-17 15:00:00"
          },
          {
            "dt": 1671300000,
            "main": {
              "temp": 0.96,
              "feels_like": -1.43,
              "temp_min": 0.96,
              "temp_max": 0.96,
              "pressure": 1023,
              "sea_level": 1023,
              "grnd_level": 1020,
              "humidity": 89,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 99
            },
            "wind": {
              "speed": 2.08,
              "deg": 188,
              "gust": 3.79
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-17 18:00:00"
          },
          {
            "dt": 1671310800,
            "main": {
              "temp": 0.21,
              "feels_like": -1.88,
              "temp_min": 0.21,
              "temp_max": 0.21,
              "pressure": 1024,
              "sea_level": 1024,
              "grnd_level": 1020,
              "humidity": 84,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 96
            },
            "wind": {
              "speed": 1.75,
              "deg": 168,
              "gust": 4.35
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-17 21:00:00"
          },
          {
            "dt": 1671321600,
            "main": {
              "temp": -0.16,
              "feels_like": -3.16,
              "temp_min": -0.16,
              "temp_max": -0.16,
              "pressure": 1024,
              "sea_level": 1024,
              "grnd_level": 1020,
              "humidity": 78,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 803,
                "main": "Clouds",
                "description": "broken clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 57
            },
            "wind": {
              "speed": 2.45,
              "deg": 143,
              "gust": 8.95
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-18 00:00:00"
          },
          {
            "dt": 1671332400,
            "main": {
              "temp": -0.03,
              "feels_like": -3.35,
              "temp_min": -0.03,
              "temp_max": -0.03,
              "pressure": 1023,
              "sea_level": 1023,
              "grnd_level": 1019,
              "humidity": 76,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 803,
                "main": "Clouds",
                "description": "broken clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 60
            },
            "wind": {
              "speed": 2.8,
              "deg": 137,
              "gust": 10.34
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-18 03:00:00"
          },
          {
            "dt": 1671343200,
            "main": {
              "temp": 0.74,
              "feels_like": -3.28,
              "temp_min": 0.74,
              "temp_max": 0.74,
              "pressure": 1021,
              "sea_level": 1021,
              "grnd_level": 1017,
              "humidity": 69,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 803,
                "main": "Clouds",
                "description": "broken clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 80
            },
            "wind": {
              "speed": 3.86,
              "deg": 145,
              "gust": 12.87
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-18 06:00:00"
          },
          {
            "dt": 1671354000,
            "main": {
              "temp": 1.62,
              "feels_like": -2.46,
              "temp_min": 1.62,
              "temp_max": 1.62,
              "pressure": 1019,
              "sea_level": 1019,
              "grnd_level": 1016,
              "humidity": 67,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 4.26,
              "deg": 153,
              "gust": 13.18
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-18 09:00:00"
          },
          {
            "dt": 1671364800,
            "main": {
              "temp": 2.53,
              "feels_like": -1.53,
              "temp_min": 2.53,
              "temp_max": 2.53,
              "pressure": 1016,
              "sea_level": 1016,
              "grnd_level": 1013,
              "humidity": 70,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 4.59,
              "deg": 158,
              "gust": 12.96
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-18 12:00:00"
          },
          {
            "dt": 1671375600,
            "main": {
              "temp": 3.44,
              "feels_like": -0.55,
              "temp_min": 3.44,
              "temp_max": 3.44,
              "pressure": 1014,
              "sea_level": 1014,
              "grnd_level": 1011,
              "humidity": 95,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 4.88,
              "deg": 165,
              "gust": 14.55
            },
            "visibility": 10000,
            "pop": 0.88,
            "rain": {
              "3h": 0.53
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-18 15:00:00"
          },
          {
            "dt": 1671386400,
            "main": {
              "temp": 6.26,
              "feels_like": 2.39,
              "temp_min": 6.26,
              "temp_max": 6.26,
              "pressure": 1012,
              "sea_level": 1012,
              "grnd_level": 1008,
              "humidity": 96,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 6.28,
              "deg": 172,
              "gust": 15.65
            },
            "visibility": 10000,
            "pop": 0.92,
            "rain": {
              "3h": 1.05
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-18 18:00:00"
          },
          {
            "dt": 1671397200,
            "main": {
              "temp": 7.58,
              "feels_like": 4.14,
              "temp_min": 7.58,
              "temp_max": 7.58,
              "pressure": 1010,
              "sea_level": 1010,
              "grnd_level": 1007,
              "humidity": 95,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 6.08,
              "deg": 177,
              "gust": 15.63
            },
            "visibility": 10000,
            "pop": 1,
            "rain": {
              "3h": 2.99
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-18 21:00:00"
          },
          {
            "dt": 1671408000,
            "main": {
              "temp": 9.25,
              "feels_like": 6.29,
              "temp_min": 9.25,
              "temp_max": 9.25,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 95,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 5.99,
              "deg": 191,
              "gust": 16.42
            },
            "visibility": 10000,
            "pop": 1,
            "rain": {
              "3h": 2.51
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-19 00:00:00"
          },
          {
            "dt": 1671418800,
            "main": {
              "temp": 10.62,
              "feels_like": 10.17,
              "temp_min": 10.62,
              "temp_max": 10.62,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 93,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 7.19,
              "deg": 206,
              "gust": 17.71
            },
            "visibility": 10000,
            "pop": 1,
            "rain": {
              "3h": 2.53
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-19 03:00:00"
          },
          {
            "dt": 1671429600,
            "main": {
              "temp": 10.88,
              "feels_like": 10.45,
              "temp_min": 10.88,
              "temp_max": 10.88,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 93,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 7.02,
              "deg": 206,
              "gust": 17.48
            },
            "visibility": 10000,
            "pop": 1,
            "rain": {
              "3h": 0.79
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-19 06:00:00"
          },
          {
            "dt": 1671440400,
            "main": {
              "temp": 10.23,
              "feels_like": 9.66,
              "temp_min": 10.23,
              "temp_max": 10.23,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 90,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 7.32,
              "deg": 197,
              "gust": 18.02
            },
            "visibility": 10000,
            "pop": 0.82,
            "rain": {
              "3h": 0.62
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-19 09:00:00"
          },
          {
            "dt": 1671451200,
            "main": {
              "temp": 10.29,
              "feels_like": 9.75,
              "temp_min": 10.29,
              "temp_max": 10.29,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 91,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 7.55,
              "deg": 200,
              "gust": 17.81
            },
            "visibility": 10000,
            "pop": 0.94,
            "rain": {
              "3h": 1.49
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-19 12:00:00"
          },
          {
            "dt": 1671462000,
            "main": {
              "temp": 11.23,
              "feels_like": 10.81,
              "temp_min": 11.23,
              "temp_max": 11.23,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 92,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 7.85,
              "deg": 201,
              "gust": 17.27
            },
            "visibility": 10000,
            "pop": 0.3,
            "rain": {
              "3h": 0.16
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-19 15:00:00"
          },
          {
            "dt": 1671472800,
            "main": {
              "temp": 12.25,
              "feels_like": 11.8,
              "temp_min": 12.25,
              "temp_max": 12.25,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 87,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 8.17,
              "deg": 201,
              "gust": 17.82
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-19 18:00:00"
          },
          {
            "dt": 1671483600,
            "main": {
              "temp": 12.38,
              "feels_like": 11.89,
              "temp_min": 12.38,
              "temp_max": 12.38,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 85,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 8.58,
              "deg": 204,
              "gust": 17.98
            },
            "visibility": 10000,
            "pop": 0.04,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-19 21:00:00"
          },
          {
            "dt": 1671494400,
            "main": {
              "temp": 11.97,
              "feels_like": 11.42,
              "temp_min": 11.97,
              "temp_max": 11.97,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 84,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 7.63,
              "deg": 202,
              "gust": 17.24
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-20 00:00:00"
          },
          {
            "dt": 1671505200,
            "main": {
              "temp": 11.5,
              "feels_like": 11.03,
              "temp_min": 11.5,
              "temp_max": 11.5,
              "pressure": 1007,
              "sea_level": 1007,
              "grnd_level": 1004,
              "humidity": 89,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 7.39,
              "deg": 196,
              "gust": 15.93
            },
            "visibility": 10000,
            "pop": 0.37,
            "rain": {
              "3h": 0.15
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-20 03:00:00"
          },
          {
            "dt": 1671516000,
            "main": {
              "temp": 11.93,
              "feels_like": 11.5,
              "temp_min": 11.93,
              "temp_max": 11.93,
              "pressure": 1006,
              "sea_level": 1006,
              "grnd_level": 1003,
              "humidity": 89,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 8.62,
              "deg": 196,
              "gust": 18.17
            },
            "visibility": 10000,
            "pop": 0.57,
            "rain": {
              "3h": 1.19
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-20 06:00:00"
          },
          {
            "dt": 1671526800,
            "main": {
              "temp": 11.91,
              "feels_like": 11.45,
              "temp_min": 11.91,
              "temp_max": 11.91,
              "pressure": 1007,
              "sea_level": 1007,
              "grnd_level": 1004,
              "humidity": 88,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 6.58,
              "deg": 204,
              "gust": 14.44
            },
            "visibility": 10000,
            "pop": 0.88,
            "rain": {
              "3h": 0.56
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-20 09:00:00"
          },
          {
            "dt": 1671537600,
            "main": {
              "temp": 11.28,
              "feels_like": 10.92,
              "temp_min": 11.28,
              "temp_max": 11.28,
              "pressure": 1006,
              "sea_level": 1006,
              "grnd_level": 1003,
              "humidity": 94,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 501,
                "main": "Rain",
                "description": "moderate rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 5.29,
              "deg": 200,
              "gust": 12.82
            },
            "visibility": 8063,
            "pop": 0.86,
            "rain": {
              "3h": 3.66
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-20 12:00:00"
          },
          {
            "dt": 1671548400,
            "main": {
              "temp": 10.98,
              "feels_like": 10.61,
              "temp_min": 10.98,
              "temp_max": 10.98,
              "pressure": 1005,
              "sea_level": 1005,
              "grnd_level": 1002,
              "humidity": 95,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 3.95,
              "deg": 190,
              "gust": 10.2
            },
            "visibility": 10000,
            "pop": 0.98,
            "rain": {
              "3h": 2.21
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-20 15:00:00"
          },
          {
            "dt": 1671559200,
            "main": {
              "temp": 10.68,
              "feels_like": 10.26,
              "temp_min": 10.68,
              "temp_max": 10.68,
              "pressure": 1004,
              "sea_level": 1004,
              "grnd_level": 1001,
              "humidity": 94,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 3.15,
              "deg": 214,
              "gust": 9.51
            },
            "visibility": 10000,
            "pop": 0.98,
            "rain": {
              "3h": 0.95
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-20 18:00:00"
          },
          {
            "dt": 1671570000,
            "main": {
              "temp": 9.88,
              "feels_like": 7.86,
              "temp_min": 9.88,
              "temp_max": 9.88,
              "pressure": 1004,
              "sea_level": 1004,
              "grnd_level": 1001,
              "humidity": 82,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 3.99,
              "deg": 237,
              "gust": 9.6
            },
            "visibility": 10000,
            "pop": 0.98,
            "rain": {
              "3h": 2.01
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-20 21:00:00"
          },
          {
            "dt": 1671580800,
            "main": {
              "temp": 8.16,
              "feels_like": 4.68,
              "temp_min": 8.16,
              "temp_max": 8.16,
              "pressure": 1006,
              "sea_level": 1006,
              "grnd_level": 1003,
              "humidity": 74,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 6.67,
              "deg": 251,
              "gust": 12.02
            },
            "visibility": 10000,
            "pop": 0.82,
            "rain": {
              "3h": 0.31
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-21 00:00:00"
          },
          {
            "dt": 1671591600,
            "main": {
              "temp": 6.22,
              "feels_like": 2.28,
              "temp_min": 6.22,
              "temp_max": 6.22,
              "pressure": 1008,
              "sea_level": 1008,
              "grnd_level": 1005,
              "humidity": 70,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 802,
                "main": "Clouds",
                "description": "scattered clouds",
                "icon": "03n"
              }
            ],
            "clouds": {
              "all": 37
            },
            "wind": {
              "speed": 6.43,
              "deg": 239,
              "gust": 13.92
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-21 03:00:00"
          },
          {
            "dt": 1671602400,
            "main": {
              "temp": 5.97,
              "feels_like": 1.96,
              "temp_min": 5.97,
              "temp_max": 5.97,
              "pressure": 1010,
              "sea_level": 1010,
              "grnd_level": 1007,
              "humidity": 71,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 801,
                "main": "Clouds",
                "description": "few clouds",
                "icon": "02n"
              }
            ],
            "clouds": {
              "all": 21
            },
            "wind": {
              "speed": 6.43,
              "deg": 236,
              "gust": 13.99
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-21 06:00:00"
          },
          {
            "dt": 1671613200,
            "main": {
              "temp": 6.12,
              "feels_like": 2.07,
              "temp_min": 6.12,
              "temp_max": 6.12,
              "pressure": 1012,
              "sea_level": 1012,
              "grnd_level": 1009,
              "humidity": 72,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 800,
                "main": "Clear",
                "description": "clear sky",
                "icon": "01d"
              }
            ],
            "clouds": {
              "all": 9
            },
            "wind": {
              "speed": 6.66,
              "deg": 235,
              "gust": 14.32
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-21 09:00:00"
          },
          {
            "dt": 1671624000,
            "main": {
              "temp": 7.79,
              "feels_like": 4.02,
              "temp_min": 7.79,
              "temp_max": 7.79,
              "pressure": 1013,
              "sea_level": 1013,
              "grnd_level": 1009,
              "humidity": 66,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 802,
                "main": "Clouds",
                "description": "scattered clouds",
                "icon": "03d"
              }
            ],
            "clouds": {
              "all": 33
            },
            "wind": {
              "speed": 7.26,
              "deg": 239,
              "gust": 14.74
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-21 12:00:00"
          },
          {
            "dt": 1671634800,
            "main": {
              "temp": 8.16,
              "feels_like": 4.51,
              "temp_min": 8.16,
              "temp_max": 8.16,
              "pressure": 1013,
              "sea_level": 1013,
              "grnd_level": 1010,
              "humidity": 67,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 802,
                "main": "Clouds",
                "description": "scattered clouds",
                "icon": "03d"
              }
            ],
            "clouds": {
              "all": 30
            },
            "wind": {
              "speed": 7.21,
              "deg": 242,
              "gust": 16.39
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2022-12-21 15:00:00"
          },
          {
            "dt": 1671645600,
            "main": {
              "temp": 7.95,
              "feels_like": 4.44,
              "temp_min": 7.95,
              "temp_max": 7.95,
              "pressure": 1013,
              "sea_level": 1013,
              "grnd_level": 1010,
              "humidity": 80,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 802,
                "main": "Clouds",
                "description": "scattered clouds",
                "icon": "03n"
              }
            ],
            "clouds": {
              "all": 47
            },
            "wind": {
              "speed": 6.57,
              "deg": 233,
              "gust": 14.75
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-21 18:00:00"
          },
          {
            "dt": 1671656400,
            "main": {
              "temp": 9.36,
              "feels_like": 6.41,
              "temp_min": 9.36,
              "temp_max": 9.36,
              "pressure": 1011,
              "sea_level": 1011,
              "grnd_level": 1008,
              "humidity": 84,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 6.05,
              "deg": 218,
              "gust": 14.8
            },
            "visibility": 10000,
            "pop": 0.24,
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-21 21:00:00"
          },
          {
            "dt": 1671667200,
            "main": {
              "temp": 9.26,
              "feels_like": 5.54,
              "temp_min": 9.26,
              "temp_max": 9.26,
              "pressure": 1006,
              "sea_level": 1006,
              "grnd_level": 1003,
              "humidity": 89,
              "temp_kf": 0
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10n"
              }
            ],
            "clouds": {
              "all": 100
            },
            "wind": {
              "speed": 8.67,
              "deg": 203,
              "gust": 19.48
            },
            "visibility": 10000,
            "pop": 0.94,
            "rain": {
              "3h": 1.58
            },
            "sys": {
              "pod": "n"
            },
            "dt_txt": "2022-12-22 00:00:00"
          }
        ],
        "city": {
          "id": 2643743,
          "name": "London",
          "coord": {
            "lat": 51.5085,
            "lon": -0.1257
          },
          "country": "GB",
          "population": 1000000,
          "timezone": 0,
          "sunrise": 1671264076,
          "sunset": 1671292313
        }
      })
    )
  }),
]
