export type ApiResponse = {
  cod: string
  message: number
  cnt: number
  list: List[]
  city: City
}

export type List = {
  dt: number
  main: Main
  weather: Weather[]
  clouds: Clouds
  wind: Wind
  visibility: number
  pop: number
  sys: Sys
  dt_txt: string
  snow?: Snow
  rain?: Rain
}

type Main = {
  temp: number
  feels_like: number
  temp_min: number
  temp_max: number
  pressure: number
  sea_level: number
  grnd_level: number
  humidity: number
  temp_kf: number
}

export type Weather = {
  id: number
  main: string
  description: string
  icon: string
}

type Clouds = {
  all: number
}

type Wind = {
  speed: number
  deg: number
  gust: number
}

type Sys = {
  pod: string
}

type Snow = {
  "3h": number
}

type Rain = {
  "3h": number
}

type City = {
  id: number
  name: string
  coord: Coord
  country: string
  population: number
  timezone: number
  sunrise: number
  sunset: number
}

type Coord = {
  lat: number
  lon: number
}

export type GroupDay = {
  short: string,
  long: string,
}

export type Group = {
  [key: string]: GroupDayObject
}

export type GroupDayObject = {
  day: GroupDay,
  data: List[],
  weather: {
    main: string,
  },
  temp: {
    min: number,
    max: number
  },
}

export type GroupKey = "today" | "tomorrow" | "daythree" | "dayfour" | "dayfive"
