import {
  getAverageWeatherForcast,
  getDayName,
  getHourlyForcastForADay,
  getMinTempAndMaxTemp
} from "../helpers"
import type { ApiResponse, Group, List } from "../types"

function factory(list: List[], datetime: string) {
  return {
    day: {
      short: getDayName(datetime, "short"),
      long: getDayName(datetime, "long"),
    },
    data: getHourlyForcastForADay(list, datetime),
    weather: {
      main: getAverageWeatherForcast(getHourlyForcastForADay(list, datetime)),
    },
    temp: {
      max: getMinTempAndMaxTemp(getHourlyForcastForADay(list, datetime)).max,
      min: getMinTempAndMaxTemp(getHourlyForcastForADay(list, datetime)).min,
    },
  }
}

export function prepareData(json: ApiResponse) {
  const { list } = json

  const datetime = new Set<string>([])
  for (const { dt_txt } of list) {
    datetime.add(`${new Date(dt_txt).getFullYear()}-${new Date(dt_txt).getMonth() + 1}-${new Date(dt_txt).getDate()}`)
  }

  const datetimeArray = Array.from(datetime)

  let groups: Group = {}
  groups.today = factory(list, datetimeArray[0])
  groups.tomorrow = factory(list, datetimeArray[1])
  groups.daythree = factory(list, datetimeArray[2])
  groups.dayfour = factory(list, datetimeArray[3])
  groups.dayfive = factory(list, datetimeArray[4])

  return groups
}
