import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { rest } from 'msw';
import App from './App';
import { getApiUrl } from './helpers';
import { server } from './mocks/server';

describe("test the whole application", () => {

  beforeEach(() => {
    render(<App />);
  })

  test('render the application', () => {
    expect(getSearchInput()).toBeInTheDocument();
    expect(getWelcomeMessage()).toBeInTheDocument();
  });

  test('show welcome message if city has been searched', () => {
    expect(getWelcomeMessage()).toBeInTheDocument();
  });

  test("show loading message", async () => {
    user.type(getSearchInput(), "london")
    user.click(getSearchButton())

    expect(
      await screen.findByText(/loading/i)
    ).toBeInTheDocument()
  })

  test("searched a city and display the results", async () => {
    user.type(getSearchInput(), "london")
    user.click(getSearchButton())

    expect(
      await screen.findByRole('heading', { name: /london/i })
    ).toBeInTheDocument()

    expect(
      await screen.findByRole('heading', { name: /today\-clouds/i })
    ).toBeInTheDocument()

    expect(
      await screen.findByRole('heading', { name: /sun\-clouds/i })
    ).toBeInTheDocument()

    expect(
      await screen.findByRole('heading', { name: /mon\-rain/i })
    ).toBeInTheDocument()

  })

  test("press the button to show a full 5 days of weather forecast", async () => {
    user.type(getSearchInput(), "london")
    user.click(getSearchButton())

    expect(
      await screen.findByRole('heading', { name: /london/i })
    ).toBeInTheDocument()

    expect(
      await screen.findByRole('heading', { name: /today\-clouds/i })
    ).toBeInTheDocument()

    expect(
      await screen.findByRole('heading', { name: /sun\-clouds/i })
    ).toBeInTheDocument()

    expect(
      await screen.findByRole('heading', { name: /mon\-rain/i })
    ).toBeInTheDocument()

    expect(
      getShowFullForecastButton()
    ).toBeInTheDocument()

    user.click(getShowFullForecastButton())

    expect(
      await screen.findByRole('heading', { name: /tue\-rain/i })
    ).toBeInTheDocument()

    expect(
      await screen.findByRole('heading', { name: /wed\-clouds/i })
    ).toBeInTheDocument()

  })

  test("if the city is not found display message", async () => {
    server.use(
      rest.get(getApiUrl("london"), (_, res, ctx) => {
        return res(
          ctx.status(404),
          ctx.json({
            "cod": "404",
            "message": "City not found"
          })
        )
      })
    )

    user.type(getSearchInput(), "london")
    user.click(getSearchButton())

    expect(
      await screen.findByText(/the city you searched for could not be found try another!/i)
    ).toBeInTheDocument()

  })

  test("if error display message", async () => {
    server.use(
      rest.get(getApiUrl("abc"), (_, res, ctx) => {
        return res(
          ctx.status(401),
          ctx.json({
            "cod": 401,
            "message": "Invalid API key. Please see https://openweathermap.org/faq#error401 for more info."
          })
        )
      })
    )

    user.type(getSearchInput(), "london")
    user.click(getSearchButton())

    expect(
      await screen.findByText(/something went wrong!/i)
    ).toBeInTheDocument()

  })

})

function getSearchInput() {
  return screen.getByPlaceholderText(/search a city/i)
}

function getWelcomeMessage() {
  return screen.getByText(/Search for your favourite city to get real time weather/i)
}

function getSearchButton() {
  return screen.getByRole('button', { name: /search/i })
}

function getShowFullForecastButton() {
  return screen.getByRole('button', { name: /show full forcast/i })
}

