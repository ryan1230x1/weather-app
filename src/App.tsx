import { useState, FormEvent } from "react"
import { prepareData } from "./utils/prepareData"
import type { ApiResponse, Group, GroupKey } from "./types"
import {
  formatDateToHourAmOrPm,
  getApiUrl,
  getWeatherIcon,
  getWeatherShortDescription,
} from "./helpers"
import ForecastDaySummary from "./components/ForecastDaySummary"
import WeatherIcon from "./components/WeatherIcon"
import Modal from "./components/Modal"
import { Arrow } from "./assets/icons/Arrow/Arrow"

const LocationSet = new Set<string>()
const locationListLimit = 5

function App() {
  const [input, setInput] = useState("")
  const [searchedCity, setSearchedCity] = useState<ApiResponse>()
  const [days, setDays] = useState<Group>({})
  const [showFullForcast, setFullForcast] = useState(false)
  const [error, setError] = useState(false)
  const [locationFound, setLocationFound] = useState(true)
  const [isLoading, setIsLoading] = useState(false)
  const [locationList, setLocationList] = useState<string[]>([])
  const [forecastDayDetails, setForecastDayDetails] = useState("")
  const [isOpenDetailModal, setIsOpenDetailModal] = useState(false)

  function handleShowFullForcast() {
    setFullForcast(true)
  }

  function handleOnCloseForecastDetailModal() {
    setIsOpenDetailModal(false)
  }

  function handleForecaseDayDetails(day: GroupKey) {
    return function() {
      setIsOpenDetailModal(true)
      setForecastDayDetails(day)
    }
  }

  function handleOnSubmit(e: FormEvent) {
    e.preventDefault()

    setIsLoading(true)
    setError(false)
    setSearchedCity(undefined)
    setLocationFound(false)

    fetch(getApiUrl(input))
      .then((response) => response.json())
      .then((json: ApiResponse) => {
        if (json) {
          if (json.cod === "200") {
            setLocationFound(true)
            setSearchedCity(json)
            setDays(prepareData(json))
            setIsLoading(false)
            LocationSet.add(input)
            setLocationList(
              Array.from(LocationSet).reverse().slice(0, locationListLimit)
            )
            setInput("")
          } else if (json.cod === "404") {
            setIsLoading(false)
          } else {
            setError(true)
            setIsLoading(false)
          }
        }
      })
      .catch((error) => {
        if (error) {
          setError(true)
          setIsLoading(false)
        }
      })
  }

  function handleOnRevisitLocation(location: string) {
    return function() {
      setIsLoading(true)
      setError(false)
      setSearchedCity(undefined)
      setLocationFound(false)

      fetch(getApiUrl(location))
        .then((response) => response.json())
        .then((json: ApiResponse) => {
          if (json) {
            if (json.cod === "200") {
              setLocationFound(true)
              setSearchedCity(json)
              setDays(prepareData(json))
              setIsLoading(false)
            } else if (json.cod === "404") {
              setIsLoading(false)
            } else {
              setError(true)
              setIsLoading(false)
            }
          }
        })
        .catch((error) => {
          if (error) {
            setIsLoading(false)
            setError(true)
          }
        })
    }
  }

  if (isLoading) {
    return (
      <div className="loader">
        <p className="text-black">...loading</p>
      </div>
    )
  }

  return (
    <main>
      <div className="form-container">
        <form className="d-flex" onSubmit={handleOnSubmit}>
          <div className="input-wrapper">
            <input
              type="text"
              value={input}
              placeholder="Search a city..."
              onChange={(e) => setInput(e.target.value)}
            />
          </div>
          <button>search</button>
        </form>
      </div>
      <div className="container">
        {error ? (
          <p className="text-center text-black">
            something went wrong!
          </p>
        ) : (
          !locationFound ? (
            <p className="text-center text-black">
              The city you searched for could not be found try another!
            </p>
          ) : (
            searchedCity ? (
              <>
                <div className="section locations-list">
                  <h1 className="text-center locations-list-title">
                    Recent Searched Cities
                  </h1>
                  <ol>
                    {locationList.map((location) => (
                      <li onClick={handleOnRevisitLocation(location)}>
                        <p>{location}</p>
                      </li>
                    ))}
                  </ol>
                </div>
                <div className="section city-description">
                  <h1 className="text-center city-name">
                    {searchedCity?.city?.name}
                  </h1>
                  <h3 className="text-center city-temperature">
                    {Math.floor(days.today?.data[0]?.main?.temp)}
                    <span className="celcius">&deg;</span>
                    {" "}
                    {getWeatherShortDescription(days.today?.weather?.main)}
                  </h3>
                </div>
                <section className="section">
                  <h2 className="section-title">Today's Weather Details</h2>
                  <div className="today-weather-details">
                    <div className="row">
                      <div className="col-6">
                        <div className="today-weather-details-group">
                          <p>Feels like</p>
                          <small>
                            {Math.floor(days.today?.data[0]?.main?.feels_like)}
                            <span>&deg;</span>
                          </small>
                        </div>
                        <div className="today-weather-details-group">
                          <p>Wind Speed</p>
                          <small>{days.today?.data[0]?.wind?.speed} km/h</small>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="today-weather-details-group">
                          <p>Humidity</p>
                          <small>{days.today.data[0].main.humidity} %</small>
                        </div>
                        <div className="today-weather-details-group">
                          <p>Pressure</p>
                          <small>{days.today?.data[0].main.pressure} mbar</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section className="section">
                  <h2 className="section-title">3 Hour Forcast</h2>
                  <div className="slider">
                    {days.today?.data.map((today) => (
                      <div key={today.dt_txt}>
                        <h5>{formatDateToHourAmOrPm(today.dt_txt)}</h5>
                        <p>{Math.floor(today.main.temp)}<span>&deg;</span></p>
                        <WeatherIcon
                          shortDescription={getWeatherShortDescription(days.today.weather.main)}
                          icon={getWeatherIcon(days.today?.weather?.main)}
                        />
                        <small className="d-flex align-items-center">
                          <Arrow deg={today.wind.deg} />{today.wind.speed} km/h
                        </small>
                      </div>
                    ))}
                  </div>
                </section>
                <section className="section">
                  <div className="five-day-forcast-wrapper">
                    <h2 className="section-title">5-day forcast</h2>
                    <ForecastDaySummary
                      title="Today"
                      icon={days.today.weather.main}
                      minTemperature={days.today.temp?.min}
                      maxTemperature={days.today.temp?.max}
                      handleShowDetails={handleForecaseDayDetails("today")}
                    />
                    <ForecastDaySummary
                      title={days.tomorrow?.day?.short}
                      icon={days.tomorrow.weather.main}
                      minTemperature={days.tomorrow.temp?.min}
                      maxTemperature={days.tomorrow.temp?.max}
                      handleShowDetails={handleForecaseDayDetails("tomorrow")}
                    />
                    <ForecastDaySummary
                      title={days.daythree?.day?.short}
                      icon={days.daythree.weather.main}
                      minTemperature={days.daythree.temp?.min}
                      maxTemperature={days.daythree.temp?.max}
                      handleShowDetails={handleForecaseDayDetails("daythree")}
                    />
                    {showFullForcast && (
                      <>
                        <ForecastDaySummary
                          title={days.dayfour?.day?.short}
                          icon={days.dayfour.weather.main}
                          minTemperature={days.dayfour.temp?.min}
                          maxTemperature={days.dayfour.temp?.max}
                          handleShowDetails={handleForecaseDayDetails("dayfour")}
                        />
                        <ForecastDaySummary
                          icon={days.dayfive.weather.main}
                          title={days.dayfive?.day?.short}
                          minTemperature={days.dayfive.temp?.min}
                          maxTemperature={days.dayfive.temp?.max}
                          handleShowDetails={handleForecaseDayDetails("dayfive")}
                        />
                      </>
                    )}
                  </div>
                </section>
                {!showFullForcast && (
                  <div className="full-forecast">
                    <button type="button" onClick={handleShowFullForcast}>
                      show full forcast
                    </button>
                  </div>
                )}
              </>
            ) : (
              <p className="text-center text-black">
                Search for your favourite city to get real time weather
              </p>
            ))
        )}

        <Modal
          isOpen={isOpenDetailModal}
          onClose={handleOnCloseForecastDetailModal}
          data={days[forecastDayDetails]}
          header={(data) => (
            <div>
              <h1>Forcast for {data?.day.long}</h1>
            </div>
          )}
          body={(data) => (
            data && (
              <>
                <div className="slider modal-slider">
                  {data.data.map((today) => (
                    <div key={today.dt_txt}>
                      <h5>{formatDateToHourAmOrPm(today.dt_txt)}</h5>
                      <p>{Math.floor(today.main.temp)}<span>&deg;</span></p>
                      <WeatherIcon
                        shortDescription={getWeatherShortDescription(days.today.weather.main)}
                        icon={getWeatherIcon(days.today?.weather?.main)}
                      />
                      <small className="d-flex align-items-center">
                        <Arrow deg={today.wind.deg} /> {today.wind.speed} km/h
                      </small>
                    </div>
                  ))}
                </div>
              </>
            )
          )}
        />

      </div>
    </main>
  );
}

export default App;
