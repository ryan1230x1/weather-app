# Real Time Weather Application

This project is a real time weather application using the open weather API

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Live Demo is available on the following URL [https://astonishing-kangaroo-53f249.netlify.app/](https://astonishing-kangaroo-53f249.netlify.app/)

## Getting Started

To get started you need to install the projects npm dependencies by running the command
```bash
npm install
```
Once the dependencies are finished installing the application can be run on your local environment with the following command
```bash
npm run start
```
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Additional Scripts

### `npm test`

Launches the test runner in the interactive watch mode.\

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
